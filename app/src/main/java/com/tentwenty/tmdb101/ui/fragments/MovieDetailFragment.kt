package com.tentwenty.tmdb101.ui.fragments

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.tentwenty.tmdb101.MovieVideoPlayerActivity
import com.tentwenty.tmdb101.R
import com.tentwenty.tmdb101.databinding.FragmentMovieDetailBinding
import com.tentwenty.tmdb101.libs.ApiInterface
import com.tentwenty.tmdb101.libs.Utils
import com.tentwenty.tmdb101.data.model.*
import com.tentwenty.tmdb101.ui.adapters.MoviesViewPagerAdapter
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


private const val ARG_MOVIE = "movie"

/**
 * A simple [Fragment] subclass.
 * Use the [MovieDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

@AndroidEntryPoint
class MovieDetailFragment : Fragment() {


    private lateinit var _binding: FragmentMovieDetailBinding
    private val binding get() = _binding

    private var movie: Movie? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            movie = it.get(ARG_MOVIE) as Movie?
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        _binding = FragmentMovieDetailBinding.inflate(inflater, container, false)
        setUpViewPager()

        Log.e("movie_selected", movie.toString())

        movie?.let { populateMovieData(it) }

        return binding.root
    }

    private fun setUpViewPager() {
        binding.tabDots.setupWithViewPager(binding.pager, true)
    }

    private fun populateMovieData(movie: Movie) {

        with(binding) {

            txtMovieTitle.text = movie.title
            txtMovieGenres.text = Utils.getGenres(movie.genre_ids)
            txtMovieReleaseDate.text = movie.release_date
            txtMovieOverview.text = movie.overview

            val adapter = MoviesViewPagerAdapter()
            val movieImageList: List<String> = listOf(movie.poster_path)
            adapter.createAdapter(activity, movieImageList)

            pager.adapter = adapter

            btnWatchTrailer.setOnClickListener {
                callGetMovieTrailerApi(movie.id)
            }

        }

    }

    private fun callGetMovieTrailerApi(movieId: Int?) {

        binding.btnWatchTrailer.text = "Loading..."
        binding.btnWatchTrailer.isEnabled = false
        val apiInterface = ApiInterface.create().getMovieTrailer(movieId, Utils.TMDb_api_key)

        apiInterface.enqueue(object : Callback<MovieTrailerData> {
            override fun onResponse(
                call: Call<MovieTrailerData>?,
                response: Response<MovieTrailerData>?
            ) {

                binding.btnWatchTrailer.text = getString(R.string.text_watch_trailer)
                binding.btnWatchTrailer.isEnabled = true

                Log.e("response", "called")
                if (response?.body() != null) {
                    val trailerList = response.body()!!.results as ArrayList<Trailer>
                    Log.e("trailerList", trailerList.toString())

                    // watchYoutubeVideo(trailerList[0].key)

                    val mIntent = Intent(activity, MovieVideoPlayerActivity::class.java)
                    mIntent.putExtra("youtubeVideoId", trailerList[0].key)
                    startActivity(mIntent)

                } else {
                    Toast.makeText(
                        activity,
                        R.string.unknown_error,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<MovieTrailerData>?, t: Throwable?) {

                binding.btnWatchTrailer.text = getString(R.string.text_watch_trailer)
                binding.btnWatchTrailer.isEnabled = true
                Toast.makeText(activity, t?.localizedMessage, Toast.LENGTH_LONG).show()
                Toast.makeText(activity, R.string.please_try_again, Toast.LENGTH_SHORT).show()
                Log.e("onFailure", "called")
            }
        })
    }

    override fun onResume() {
        super.onResume()
        Log.e("onResume", "called")

        binding.btnWatchTrailer.text = getString(R.string.text_watch_trailer)
        binding.btnWatchTrailer.isEnabled = true

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param movie Parameter 1.
         * @return A new instance of fragment MovieDetailFragment.
         */
        @JvmStatic
        fun newInstance(movie: Movie) =
            MovieDetailFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_MOVIE, movie)
                }
            }
    }

    private fun watchYoutubeVideo(id: String) {
        val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$id"))
        val webIntent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("http://www.youtube.com/watch?v=$id")
        )
        try {
            startActivity(appIntent)
        } catch (ex: ActivityNotFoundException) {
            startActivity(webIntent)
        }
    }
}