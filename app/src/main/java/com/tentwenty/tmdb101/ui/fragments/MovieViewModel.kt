package com.tentwenty.tmdb101.ui.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.tentwenty.tmdb101.data.MovieRepository
import com.tentwenty.tmdb101.data.model.Movie
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val _movieRepository: MovieRepository
) : ViewModel() {

    private val movieRepository = _movieRepository
    private val movies: LiveData<List<Movie>> = movieRepository.getAllMovies()

    fun insert(cats: ArrayList<Movie>) {
        movieRepository.insert(cats)
    }

    fun getAllMovies(): LiveData<List<Movie>> {
        return movies
    }

}