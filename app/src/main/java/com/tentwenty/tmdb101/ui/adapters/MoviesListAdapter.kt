package com.tentwenty.tmdb101.ui.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tentwenty.tmdb101.MainActivity
import com.tentwenty.tmdb101.MovieTicketBookingActivity
import com.tentwenty.tmdb101.R
import com.tentwenty.tmdb101.databinding.MoviesListItemBinding
import com.tentwenty.tmdb101.libs.Utils
import com.tentwenty.tmdb101.data.model.Movie
import com.tentwenty.tmdb101.ui.fragments.MovieDetailFragment


class MoviesListAdapter(private var activity: MainActivity, private var moviesList: List<Movie>) :

    RecyclerView.Adapter<MoviesListAdapter.MyViewHolder>() {

    lateinit var recyclerView: RecyclerView

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {

        this.recyclerView = recyclerView
        super.onAttachedToRecyclerView(recyclerView)
    }

    fun setMovies(moviesList: List<Movie>) {
        this.moviesList = moviesList
        notifyDataSetChanged()
    }

    fun getAllMovies() {

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding = MoviesListItemBinding.inflate(inflater)

        binding.root.setOnClickListener {
            val itemPosition: Int = recyclerView.getChildLayoutPosition(it)
            val movie = moviesList[itemPosition]

            activity.replaceFragment(
                R.id.frame_container,
                MovieDetailFragment.newInstance(movie),
                "FragmentMovieDetail",
                "FragmentMovieDetail-OPEN"
            )
        }

        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) =
        holder.bind(moviesList[position])

    override fun getItemCount(): Int {
        return moviesList.size
    }

    inner class MyViewHolder(private val binding: MoviesListItemBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {

        fun bind(movie: Movie) {
            with(binding) {
                txtMovieTitle.text = movie.title
                txtMovieDateRelease.text = movie.release_date
                txtIsAdult.text = if (movie.adult) "Adult" else "Non Adult"

                Glide.with(activity)
                    .load(Utils.getMovieImageUrl(movie.poster_path))
                    .placeholder(R.drawable.img_placeholder_loading)
                    .error(R.drawable.img_error)
                    .into(imgMovieThumbnail)

                btnBook.setOnClickListener {
                    val mIntent = Intent(activity, MovieTicketBookingActivity::class.java)
                    mIntent.putExtra("MOVIE", movie)
                    activity.startActivity(mIntent)
                }
            }
        }
    }
}