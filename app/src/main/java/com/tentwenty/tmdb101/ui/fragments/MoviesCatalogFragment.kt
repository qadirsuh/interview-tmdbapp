package com.tentwenty.tmdb101.ui.fragments

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.tentwenty.tmdb101.BaseApp
import com.tentwenty.tmdb101.MainActivity
import com.tentwenty.tmdb101.R
import com.tentwenty.tmdb101.data.MovieRepository
import com.tentwenty.tmdb101.databinding.FragmentMoviesCatalogBinding
import com.tentwenty.tmdb101.libs.ApiInterface
import com.tentwenty.tmdb101.libs.Utils
import com.tentwenty.tmdb101.data.model.Movie
import com.tentwenty.tmdb101.data.model.MoviesData
import com.tentwenty.tmdb101.roomdb.AppDatabase
import com.tentwenty.tmdb101.ui.adapters.MoviesListAdapter
import dagger.hilt.android.AndroidEntryPoint
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * A simple [Fragment] subclass.
 * Use the [MoviesCatalogFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class MoviesCatalogFragment : Fragment() {

    private lateinit var moviesListAdapter: MoviesListAdapter

    private var moviesList = ArrayList<Movie>()

    private lateinit var _binding: FragmentMoviesCatalogBinding
    private val binding get() = _binding

    private lateinit var mMovieRepository: MovieRepository

    private lateinit var mMovieViewModel: MovieViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mMovieRepository = MovieRepository(AppDatabase.getDatabase(requireActivity()).movieDao!!)
        mMovieViewModel = ViewModelProvider(this).get(MovieViewModel::class.java)

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout binding for this fragment
        _binding = FragmentMoviesCatalogBinding.inflate(inflater, container, false)

        moviesListAdapter = MoviesListAdapter(activity as MainActivity, moviesList)
        initRecyclerView()

        checkConnectivityAndGetUpcomingMovies()

        binding.swipeRefresh.setOnRefreshListener {
            checkConnectivityAndGetUpcomingMovies()
        }

        return binding.root
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun checkConnectivityAndGetUpcomingMovies() {
        // Call getMovies API

        if (Utils.isOnline(requireActivity())) {
            Log.e("isOnline", "YES")
            callGetMoviesApi()
        } else {

            binding.swipeRefresh.isRefreshing = false
            Log.e("isOnline", "NO")
            Toast.makeText(
                activity,
                "Looks you are offline! You are viewing offline data",
                Toast.LENGTH_LONG
            ).show()

            mMovieViewModel.getAllMovies().observe(viewLifecycleOwner, Observer {
                binding.recyclerView.adapter = moviesListAdapter
                moviesListAdapter.setMovies(it)
            })
        }
    }

    private fun initRecyclerView() {

        val layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = moviesListAdapter

        val dividerItemDecoration = DividerItemDecoration(
            binding.recyclerView.context,
            layoutManager.orientation
        )
        binding.recyclerView.addItemDecoration(dividerItemDecoration)
    }

    private fun callGetMoviesApi() {

        binding.swipeRefresh.isRefreshing = true
        val apiInterface = ApiInterface.create().getMovies(Utils.TMDb_api_key)

        apiInterface.enqueue(object : Callback<MoviesData> {
            override fun onResponse(call: Call<MoviesData>?, response: Response<MoviesData>?) {

                binding.swipeRefresh.isRefreshing = false
                Log.e("response", "called")
                if (response?.body() != null) {

                    moviesList = response.body()!!.results as ArrayList<Movie>
                    Log.e("moviesList", moviesList.toString())
                    moviesListAdapter.setMovies(moviesList)
                    mMovieRepository.insert(moviesList)

                } else {
                    Toast.makeText(
                        activity,
                        R.string.unknown_error,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<MoviesData>?, t: Throwable?) {
                binding.swipeRefresh.isRefreshing = false

                Log.e("onFailure", t?.localizedMessage.toString())

                Toast.makeText(activity, t?.localizedMessage, Toast.LENGTH_LONG).show()
                Toast.makeText(activity, R.string.please_try_again, Toast.LENGTH_SHORT).show()
                Log.e("onFailure", "called")
            }
        })

    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment MoviesCatalogFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            MoviesCatalogFragment().apply {}
    }
}