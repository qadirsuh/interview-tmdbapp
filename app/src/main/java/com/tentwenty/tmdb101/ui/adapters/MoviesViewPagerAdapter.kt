package com.tentwenty.tmdb101.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import android.content.Context

import android.view.LayoutInflater
import android.os.Parcelable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.tentwenty.tmdb101.R
import com.tentwenty.tmdb101.libs.Utils


class MoviesViewPagerAdapter() : PagerAdapter() {

    private lateinit var imageUrls: List<String>
    private var inflater: LayoutInflater? = null
    private var context: Context? = null

    fun createAdapter(context: Context?, imageUrls: List<String>) {
        this.context = context
        this.imageUrls = imageUrls
        inflater = LayoutInflater.from(context)
    }


    override fun getCount(): Int {
        return 3
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val rootView: View =
            inflater?.inflate(R.layout.viewpager_item, view, false)!!

        val imageView: ImageView = rootView
            .findViewById(R.id.imgMovieCover) as ImageView

        Glide.with(context!!)
            .load(Utils.getMovieImageUrl(imageUrls[0]))
            .placeholder(R.drawable.img_placeholder_loading)
            .error(R.drawable.img_error)
            .into(imageView)

        view.addView(rootView, 0)

        return rootView
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

}