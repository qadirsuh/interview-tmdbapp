package com.tentwenty.tmdb101.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.tentwenty.tmdb101.roomdb.DateTypeConverter
import java.io.Serializable

data class MoviesData(
    var page: Int,
    var results: List<Movie>,
    var total_pages: Int,
    var total_results: Int
)

@Entity(tableName = "upcoming_movies")
class Movie(

    @PrimaryKey
    @SerializedName("id")
    var id: Int,

    @SerializedName("adult")
    var adult: Boolean,

    @SerializedName("backdrop_path")
    var backdrop_path: String,

    @SerializedName("original_language")
    var original_language: String,

    @SerializedName("original_title")
    var original_title: String,

    @SerializedName("overview")
    var overview: String,

    @SerializedName("popularity")
    var popularity: Double,

    @SerializedName("poster_path")
    var poster_path: String,

    @SerializedName("release_date")
    var release_date: String,

    @SerializedName("title")
    var title: String,

    @SerializedName("video")
    var video: Boolean,

    @SerializedName("vote_average")
    var vote_average: String,

    @SerializedName("vote_count")
    var vote_count: String,

    @SerializedName("genre_ids")
    @TypeConverters(*[DateTypeConverter::class])
    var genre_ids : List<Int>
) : Serializable