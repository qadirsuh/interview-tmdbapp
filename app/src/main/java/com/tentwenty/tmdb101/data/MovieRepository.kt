package com.tentwenty.tmdb101.data

import android.util.Log
import androidx.lifecycle.LiveData
import com.tentwenty.tmdb101.BaseApp
import com.tentwenty.tmdb101.data.model.Movie
import com.tentwenty.tmdb101.roomdb.MovieDao
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val _movieDao: MovieDao
) {
    private val movieDao = _movieDao
    private var movies = movieDao.allMovies()

    fun insert(movies: ArrayList<Movie>) {

        for (movie: Movie in movies) {
            Log.e("insert", movie.title)
            movieDao.insert(movie)
        }
    }

    fun getAllMovies(): LiveData<List<Movie>> {
        return movies
    }

}