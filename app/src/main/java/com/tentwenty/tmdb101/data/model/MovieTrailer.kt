package com.tentwenty.tmdb101.data.model

import java.io.Serializable

data class MovieTrailerData(
    var id: Int,
    var results: List<Trailer>
) : Serializable

data class Trailer(

    var name: String,
    var key: String,
    var site: String,
    var size: String,
    var type: String,
    var official: Boolean,
    var published_at: String,
    var id: String
) : Serializable