package com.tentwenty.tmdb101.libs

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import org.json.JSONObject


class Utils {

    companion object {
        val TMDb_api_key: String
            get() = "1c6808a5563c154deef75a57c9c49408"

        val TMDb_movie_genre: String
            get() = "{\"genres\":[{\"id\":28,\"name\":\"Action\"},{\"id\":12,\"name\":\"Adventure\"},{\"id\":16,\"name\":\"Animation\"},{\"id\":35,\"name\":\"Comedy\"},{\"id\":80,\"name\":\"Crime\"},{\"id\":99,\"name\":\"Documentary\"},{\"id\":18,\"name\":\"Drama\"},{\"id\":10751,\"name\":\"Family\"},{\"id\":14,\"name\":\"Fantasy\"},{\"id\":36,\"name\":\"History\"},{\"id\":27,\"name\":\"Horror\"},{\"id\":10402,\"name\":\"Music\"},{\"id\":9648,\"name\":\"Mystery\"},{\"id\":10749,\"name\":\"Romance\"},{\"id\":878,\"name\":\"Science Fiction\"},{\"id\":10770,\"name\":\"TV Movie\"},{\"id\":53,\"name\":\"Thriller\"},{\"id\":10752,\"name\":\"War\"},{\"id\":37,\"name\":\"Western\"}]}"

        fun getMovieImageUrl(imageName: String): String {
            return "https://image.tmdb.org/t/p/w500$imageName"
        }

        fun getGenres(genreIds: List<Int>): String {

            Log.e("genreIds", genreIds.toString())

            var genresMatch = ArrayList<String>()
            val items = JSONObject(TMDb_movie_genre).getJSONArray("genres")

            for (i in 0 until items.length()) {
                val item = items.getJSONObject(i)

                Log.e("item", item.toString())

                if (genreIds.contains(item.getInt("id"))) {
                    Log.e("item$i match", item.toString())
                    genresMatch.add(item.getString("name"))
                }
            }

            return genresMatch.joinToString(", ")

        }

        @SuppressLint("NewApi")
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun isOnline(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (connectivityManager != null) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    when {
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                            Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                            return true
                        }
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                            Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                            return true
                        }
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                            Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                            return true
                        }
                    }
                }
            }
            return false
        }
    }
}