package com.tentwenty.tmdb101.libs

import com.tentwenty.tmdb101.data.model.MovieTrailerData
import com.tentwenty.tmdb101.data.model.MoviesData
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {

    @GET("/3/movie/upcoming")
    fun getMovies(@Query("api_key") api_key: String): Call<MoviesData>

    @GET("/3/movie/{movie_id}/videos")
    fun getMovieTrailer(
        @Path("movie_id", encoded = true) movie_id: Int?,
        @Query("api_key") api_key: String
    ): Call<MovieTrailerData>

    companion object {

        var BASE_URL = "https://api.themoviedb.org"

        fun create(): ApiInterface {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiInterface::class.java)

        }
    }

}