package com.tentwenty.tmdb101

import android.app.Application
import com.tentwenty.tmdb101.roomdb.AppDatabase
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BaseApp : Application() {

    companion object{
        val get = this
    }

    lateinit var database : AppDatabase

    override fun onCreate() {
        super.onCreate()
        database = AppDatabase.getDatabase(this)
    }
}