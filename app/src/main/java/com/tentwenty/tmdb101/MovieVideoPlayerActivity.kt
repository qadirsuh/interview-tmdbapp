package com.tentwenty.tmdb101

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.tentwenty.tmdb101.databinding.ActivityMovieVideoPlayerBinding
import com.tentwenty.tmdb101.databinding.FragmentMovieDetailBinding


class MovieVideoPlayerActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityMovieVideoPlayerBinding
    private val binding get() = _binding

    private lateinit var mIntent: Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = ActivityMovieVideoPlayerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mIntent = intent

        lifecycle.addObserver(binding.youTubePlayerView)

        binding.youTubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {

               val movieTrailerYoutubeVideoId =  mIntent.getStringExtra("youtubeVideoId")
                Log.e("YoutubeVideoIdTEST", movieTrailerYoutubeVideoId.toString())
                youTubePlayer.loadVideo(movieTrailerYoutubeVideoId!!, 0f)
            }
        })

        binding.btnDone.setOnClickListener {
            finish()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }
}