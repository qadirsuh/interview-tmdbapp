package com.tentwenty.tmdb101

import android.content.res.Configuration
import android.os.Bundle
import com.tentwenty.tmdb101.ui.fragments.MoviesCatalogFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addFragment(
            R.id.frame_container,
            MoviesCatalogFragment.newInstance(),
            "FragmentMoviesCatalog"
        )

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
    }
}