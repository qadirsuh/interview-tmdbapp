package com.tentwenty.tmdb101.roomdb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.tentwenty.tmdb101.data.model.Movie

@Database(entities = [Movie::class], version = 1)
@TypeConverters(*[DateTypeConverter::class])
abstract class AppDatabase : RoomDatabase() {
    abstract val movieDao: MovieDao?

    companion object {

        private var INSTANCE: AppDatabase? = null

        open fun getDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, "db-tmdb")
                    .allowMainThreadQueries().build()
            }
            return INSTANCE as AppDatabase
        }

        open fun destroyInstance() {
            INSTANCE = null
        }
    }


}